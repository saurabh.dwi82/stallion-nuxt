export const usePropertyStore = defineStore('property',{
    state: () => {
        return {
            baseUrl : 'http://localhost/laravel-st-test/public/api/',
            message : "",
            properties : {},
            propertyTypes : [],
            property: {},
            createProperty: {},
        }
    },
    actions: {
        async getProperties(){
            const response = await fetch(`${this.baseUrl}properties`);
            const properties = await response.json();
            this.properties = properties;
        },
        async deleteProperty(id : string){
            const response = await fetch(`${this.baseUrl}properties/${id}`,{
                method: 'DELETE'
            });
            this.message = response.message;
        },
        async getPropertyTypes(){
            const response = await fetch(`${this.baseUrl}property-types`);
            const property = await response.json();
            this.propertyTypes = property;
        },
        async createProperty(data : any){
            const response = await fetch(`${this.baseUrl}properties`,{
                headers: {
                    Accept: 'application/json',
                  },
        
                method: 'POST',
                body: data
            });
            this.createProperty = await response.json();
        },
        async getPropertyById(id : any){
            const response = await fetch(`${this.baseUrl}properties/${id}`,{
                headers: {
                    Accept: 'application/json',
                  },
        
                method: 'GET',
            });
            const property = await response.json();
            this.property = property.data;
        },
        async updateProperty(id:string,data : any){
            const response = await fetch(`${this.baseUrl}properties/${id}`,{
                headers: {
                    Accept: 'application/json',
                  },        
                method: 'POST',
                body: data
            });
        },
    }
});